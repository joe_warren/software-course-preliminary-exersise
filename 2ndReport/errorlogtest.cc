
#include <stdio.h>

#include "errorlog.h"
#include "gtest/gtest.h"

using namespace std;

namespace {


TEST(errorLogTest, singletonAccessWorks){
	
	errorLog *el = &(errorLog::getInstance()); 

	ASSERT_NE( (errorLog*)NULL, el ); 
}


TEST(inputExceptionTest, SettingErrorsWorksAcrossSingletons){
	
	
	inputException ie( inputException::INVALID_PROPERTY_NAME, 
		pair<int,int>( 2, 5 ),  "a line with an error" );

	errorLog & el1 = errorLog::getInstance();

	el1.push_back( ie );

	errorLog & el2 = errorLog::getInstance();

	EXPECT_STREQ("An invalid property name was found", 
		el2.at(0).getReasonString().c_str() );
}

};
