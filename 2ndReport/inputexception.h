#ifndef inputexception_h
#define inputexception_h

#include <stdio.h>
#include <iostream>
#include <utility>

namespace std{

class inputException{
	public:
		enum inputErrorType{
			INVALID = 0,
			MISSING_SEMICOLON,
			INVALID_TYPE_NAME,
			INVALID_DEVICE_NAME, 
			INVALID_PROPERTY_NAME, 
			INVALID_INPUT_NAME,
			KEYWORD_USED_AS_DEVICE_NAME,
			MISSING_OPEN_BRACE,
			MISSING_CLOSE_BRACE,
			MISSING_EQUALS, 
			WRONG_DEVICE_FOR_INPUT,
			DUPLICATE_DEVICE_NAME,
			UNEXISTENT_OUTPUT,
			UNEXISTENT_DEVICE,
			MISSING_PROPERTY,
			INVALID_PROPERTY_VALUE,
			TOO_MANY_MONITORS,
			MISSING_DEVICE_NAME 
		};

		/* construct a syntaxException */ 
		inputException( inputErrorType aType, pair<int, int> position,
			string aLine );

		string getReasonString();
		string getLineString ();
				
	private:
		/* default constructor, not used */
		inputException();

		/* This _MUST_ be kept up to date with the syntaxErrorType enum 
		 * or Niklaus Wirth will haunt you, and he's still alive,
		 * which makes it worse really
		 */
		static const char * REASONS[];
		static const int REASON_COUNT;

		inputErrorType theType;
		string theLine;
		int theLineNumber;
		int theCharNumber;

};

}

		
#endif /* inputexception_h */
