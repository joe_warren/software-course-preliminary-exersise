#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cctype>
#include <algorithm>
#include <vector>

#include "check.h"

using namespace std;


class namestring: public string{
  public:
    static const int maxlength;
};

const int namestring::maxlength = 8;


typedef int name;

class nametable: public vector<namestring> {
  public:
   void printContents();
   name lookup( namestring &n );
   void writename( name n );
   int namelength( name n );
};


void nametable:: printContents(){
  cout << "NameTable Contains:" << endl;
  int i = 0;
  for ( nametable::iterator it=begin(); it!=end(); ++it, i++){
    cout << "\t" << i << " : " << *it << endl;
  }
}

void nametable::writename( name n ){
  namestring ns = at( n );
  cout << ns;
}


int nametable::namelength( name n ){
  return at(n).length();
} 

name nametable::lookup( namestring &n ){

  nametable::iterator p = find( begin(), end(), n );
  if( p == end() ){ 
    push_back(n); 
    p = find( begin(), end(), n );
  } 
  return distance( begin(), p ); 
}



void skipspaces( ifstream *infp, char &curch, bool &eof ){
  eof = ( infp->get(curch) == 0 );
  while( !eof ){
    if ( !isspace(curch) ){
      return;
    }
    eof = ( infp->get(curch) == 0 );
  }
}

void getnumber( ifstream *infp, char &curch, bool &eofile, int &number ){
  number = 0;
  while( isdigit( curch ) ){
    number *= 10;
    number += curch - '0';    
    if(  eofile = ( infp->get(curch) == 0 ) ) return;
  }
}

void getname( ifstream *infp, char &curch, bool &eofile, name & n, nametable &nt ){
  namestring ns;
  ns.clear();
  ns.reserve( namestring::maxlength + 1 );
  check c;

  int count = 0;

  if( isalpha( curch ) ){
    while( isalnum( curch ) ){
      count ++;
      if( count > namestring::maxlength ){
        cerr << "OVERLONG NAME STRING" <<endl;
        while( isalnum( curch ) ){ // Skip the remaining namestring
          if(  eofile = ( infp->get(curch) == 0 ) ) return;
        }
        break; //Overlong name string, stop parsing
      }   
      ns.push_back( curch );
      if(  eofile = ( infp->get(curch) == 0 ) ) return;
    }
  } else {
    cerr << "NAMESTRING EXPECTED, may not begin with \"" << curch << "\"" << endl;
    eofile = ( infp->get(curch) == 0 );
  }
  c.ckstr( ns );
  n = nt.lookup( ns );
}



int main( int argc, char *argv[] ){
  ifstream inf;
  char ch;
  bool eofile;

  if( argc != 2 ){
    cerr << "Usage:      " << argv[0] << " [filename]" << endl;
    exit(1);
  }
  inf.open( argv[1] );
  if( !inf ){
    cerr << "Error: cannot open file " << argv[1] << "for reading " << endl;
    exit(1);
  }

  skipspaces( &inf, ch, eofile );

  int number;
  namestring nstring;
  nametable ntable;
  name n;

  while( !eofile ){
    if( isdigit( ch ) ){
      getnumber( &inf, ch, eofile, number );
      cout << "number: " << number << endl;
    } else if( isalpha( ch ) ) {
      getname( &inf, ch, eofile, n, ntable );
      cout << "name: " << n << 
              " length: "<< ntable.namelength( n ) <<
              " string: ";
      ntable.writename( n );
      cout << endl;
    }
    skipspaces( &inf, ch, eofile );
  }
  ntable.printContents();
  inf.close();
  return( 0 );
} 
