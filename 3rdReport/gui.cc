#include "gui.h"
#include <GL/glut.h>
#include <stdlib.h>
#include <time.h>
#include "wx_icon.xpm"
#include <sstream>
#include <iostream>

#include "errorlog.h"

#define I_MAX( X, Y ) ( X > Y? X: Y ) 

using namespace std;

wxTextCtrl *txtboxConsole;

enum
//EVENT NAMES, so that each control can have an ID, and then you can pair each ID to a function
// in the event table
{
  Menu_About = wxID_ABOUT,
  Menu_Quit = wxID_EXIT,

  LISTBOX_CHECK,

  //a list of events
  BUTTON_RUN,
  BUTTON_RESET,
  BUTTON_MONITOR_ADD,
  MONITOR_NEW,
  SLIDER_CHANGE,
  BUTTON_MONITOR_REMOVE
  //hack: this shall be the last one, so
  //BUTTON_MONITOR_REMOVE + i will be the remove event
  //for the corresponding
  //ith monitor.
};

const int MyGLCanvas::PERIOD = 10;

// MyGLCanvas ////////////////////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE(MyGLCanvas, wxGLCanvas)
EVT_SIZE(MyGLCanvas::OnSize)
EVT_PAINT(MyGLCanvas::OnPaint)
EVT_SCROLLWIN(MyGLCanvas::OnScroll)
EVT_SHOW(MyGLCanvas::OnShow)
END_EVENT_TABLE()

int wxglcanvas_attrib_list[5] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};

// Constructor - initialises private variables
MyGLCanvas::MyGLCanvas(wxWindow *parent, wxWindowID id,
    monitor* monitor_mod, names* names_mod,
    const wxPoint& pos, const wxSize& size, long style, const wxString& name): //this means just call parent!
      wxGLCanvas(parent, id, pos, size, style, name, wxglcanvas_attrib_list){
  mmz = monitor_mod;
  nmz = names_mod;
  init = false;
  hasBeenDrawn = false;
  cyclesdisplayed = -1;
  theParent = static_cast<MyFrame *>(parent);
}

void MyGLCanvas::setMonitorNum(int _monnum){
  monnum = _monnum;
}

// Draws canvas contents
void MyGLCanvas::Render(int cycles){
  float y;
  asignal s;

  if (cycles >= 0){
    cyclesdisplayed = cycles;
  }

  int position = GetScrollPos(wxHORIZONTAL);
  int drawable = 20 + PERIOD * cyclesdisplayed;
  if( cycles > 0 ){
    position = max(drawable - width, 0 ) ;
  } else if( cycles == 0 ) {
    position = 0;
  }
  SetScrollbar(wxHORIZONTAL, position, width, drawable );

  SetCurrent();
  if (!init) {
    InitGL();
    init = true;
  }
  glClearColor(0.0,0.0, 0.0,1.0);
  glClear(GL_COLOR_BUFFER_BIT);

  glLineStipple(1, 0xAAAA);
  glEnable(GL_LINE_STIPPLE);

  glColor3f(0.2, 0.2, 0.2);

  glBegin(GL_LINES);
  for(int i = -position%PERIOD; i < width; i += PERIOD ){
    glVertex2f(i, height);
    glVertex2f(i, 0);
  } 
  glEnd(); 	

  glLineStipple(1, 0xBBBB);
  glColor3f(0.5, 0.5, 0.5);

  glBegin(GL_LINES);
  for(int i = -position%(PERIOD*10); i < width; i += PERIOD *10){
    glVertex2f(i, height);
    glVertex2f(i, 0);
  } 
  glEnd(); 	
  glDisable(GL_LINE_STIPPLE);

  int h = height;
  if( width < drawable ){ //scrollbar is shown
    h -= wxSystemSettings::GetMetric(wxSYS_HSCROLL_Y);
  }

  if ((cyclesdisplayed >= 0) && (mmz->moncount() > 0)){ // draw the first monitor signal, get trace from monitor class

    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_LINE_STRIP);

    asignal prev = LOW;

    if( cyclesdisplayed > 0 ){
      mmz->getsignaltrace(monnum, 0, prev);
    }
    for (int i=0; i<cyclesdisplayed; i++) {
      if (mmz->getsignaltrace(monnum, i, s)) {
        if( s != prev ){
          glVertex2f(PERIOD*i+10.0 - position, h/2);
        }
        if (s==LOW ){
          y = 10.0;
          glColor3f(1.0, 1.0, 1.0);
        }
        if (s==HIGH ){
          y = h - 10;
          glColor3f(1.0, 0.0, 0.0);
        }
        if( s== (asignal)-1){
          y = h/2;
          glColor3f(0.0, 0.0, 0.2);
        }
        if( s != prev ){
          glVertex2f(PERIOD*i+10.0 - position, h/2);
        }

        glVertex2f(PERIOD*i+10.0 - position, y);
        glVertex2f(PERIOD*i+10.0+PERIOD - position, y);

        prev = s;
      }
    }
    glEnd();
  }

  // We've been drawing to the back buffer, flush the graphics pipeline and swap the back buffer to the front
  glFlush();
  SwapBuffers();
}

// Function to initialise the GL context
void MyGLCanvas::InitGL(){
  int w, h;
  GetClientSize(&w, &h);
  SetCurrent();
  glDrawBuffer(GL_BACK);
  glClearColor(1.0, 1.0, 1.0, 0.0);
  glViewport(0, 0, (GLint) w, (GLint) h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, w, 0, h, -1, 1); 
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}


void MyGLCanvas::OnShow(wxShowEvent& /*event*/){
  Render(0);
}

// Callback function for when the canvas is exposed
void MyGLCanvas::OnPaint(wxPaintEvent& /* event */){
  int w, h;
  wxString text;

  wxPaintDC dc(this); // required for correct refreshing under MS windows
  GetClientSize(&w, &h);
  Render(-1);

}

void MyGLCanvas::setSlider( int pos ){
  int drawable = 20 + PERIOD * cyclesdisplayed;
  SetScrollbar(wxHORIZONTAL, pos, width, drawable );
  Render(-1); 
}

void MyGLCanvas::OnScroll( wxScrollWinEvent& /*event*/ ){
  int pos = GetScrollPos(wxHORIZONTAL);
  setSlider( pos );
  theParent->updateGLSliders( pos );
}
// Callback function for when the canvas is resized
void MyGLCanvas::OnSize(wxSizeEvent& event){
  height = event.GetSize().GetHeight();
  width = event.GetSize().GetWidth();

  wxGLCanvas::OnSize(event); // required on some platforms
  init = false;
  Refresh(); // required by some buggy nvidia graphics drivers,
  Update();  // harmless on other platforms!

  if (hasBeenDrawn){
    Render(-1);
  }
}



// MyFrame /////////////////////////////////////////////////////////////////////////////////////////


BEGIN_EVENT_TABLE(MyFrame, wxFrame)
EVT_MENU(wxID_EXIT, MyFrame::OnExit)
EVT_MENU(wxID_ABOUT, MyFrame::OnAbout)
EVT_BUTTON(BUTTON_RUN, MyFrame::OnRunButton)
EVT_BUTTON(BUTTON_RESET, MyFrame::OnResetButton)
EVT_BUTTON(BUTTON_MONITOR_ADD, MyFrame::OnAddMonitorButton)
EVT_BUTTON(wxID_ANY, MyFrame::OnRemoveMonitorButton) //any button will trigger this, therefore must check what button in the function
EVT_CHECKLISTBOX(LISTBOX_CHECK, MyFrame::OnSwitchChange)
EVT_COMMAND_SCROLL(SLIDER_CHANGE,MyFrame::OnSliderChange)
END_EVENT_TABLE()

MyFrame::MyFrame(wxWindow *parent, const wxString& title, int lang, const wxPoint& pos, const wxSize& size,
    names *names_mod, devices *devices_mod, monitor *monitor_mod, long style ):
    wxFrame(parent, wxID_ANY, title, pos, size, style)
// Constructor - initialises pointers to names, devices and monitor classes, lays out widgets
// using sizers
{


  current_language = lang;

  if( current_language == -1 ){ 
    wxArrayString choices;
    choices.Add(wxT("English"));
    choices.Add(wxT("Malay"));
    current_language = wxGetSingleChoiceIndex( wxT("Language"), wxT("Choose Language"),choices );
  }	
  SetIcon(wxIcon(wx_icon));

  nmz = names_mod;
  dmz = devices_mod;
  mmz = monitor_mod;
  if (nmz == NULL || dmz == NULL || mmz == NULL) {
    cout << "Cannot operate GUI without names, devices and monitor classes" << endl;
    exit(1);
  }

  wxMenu *fileMenu = new wxMenu;
  fileMenu->Append(wxID_ABOUT, enumToWxString(languages::MENU_ABOUT));
  fileMenu->Append(wxID_EXIT, enumToWxString(languages::MENU_QUIT));
  wxMenuBar *menuBar = new wxMenuBar;
  menuBar->Append(fileMenu, enumToWxString(languages::MENU_FILE));
  SetMenuBar(menuBar);

  // boxes
  mainBoxSizer = new wxBoxSizer(wxVERTICAL);
  rowOneBS = new wxBoxSizer(wxHORIZONTAL);
  rowOneLeftBS = new wxBoxSizer(wxVERTICAL);
  rowTwoBS = new wxBoxSizer(wxVERTICAL);
  rowTwoPointFiveBS = new wxBoxSizer(wxHORIZONTAL);
  rowThreeBS = new wxBoxSizer(wxHORIZONTAL);
  rowFourBS = new wxBoxSizer(wxHORIZONTAL);

  //slider
  sliderCycle = new wxSlider(this, SLIDER_CHANGE,
      4,		//init
      1,		//min
      20, 	//max
      wxDefaultPosition, wxDefaultSize,
      wxSL_HORIZONTAL | wxSL_LABELS);



  //buttons
  wxButton *btnRun = new wxButton(this, BUTTON_RUN, enumToWxString(languages::LAB_RUN));
  wxButton *btnReset = new wxButton(this, BUTTON_RESET, enumToWxString(languages::LAB_RESET));
  wxButton *btnMonitorAdd = new wxButton(this, BUTTON_MONITOR_ADD, enumToWxString(languages::LAB_NEW_MONITOR));

  btnRun->SetBackgroundColour(wxColour(131,199,134));
  btnRun->SetForegroundColour(*wxWHITE);

  btnReset->SetBackgroundColour(wxColour(255,186,186));
  btnReset->SetForegroundColour(*wxWHITE);

  //checkbox
  switchList  = new wxCheckListBox
      (
          this,               // parent
          LISTBOX_CHECK,       // control id
          wxDefaultPosition,       // listbox poistion
          wxDefaultSize,      // listbox size
          0,  // number of strings
          0,           // array of strings
          0
      );


  //console
  wxTextCtrl *txtboxConsole = new wxTextCtrl(this,
      wxID_ANY, wxT(""),
      wxDefaultPosition,
      wxDefaultSize,
      wxTE_MULTILINE | wxHSCROLL | wxTE_READONLY);

  wxFont * monofont = new wxFont(10,wxFONTFAMILY_TELETYPE,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,false,wxEmptyString,wxFONTENCODING_DEFAULT);
  txtboxConsole->SetFont(*monofont);
  txtTarget = new wxLogTextCtrl(txtboxConsole);
  wxLog::SetActiveTarget(txtTarget);

  // ROW ONE: First row has slider and two buttons-----------------------

  //buttons


  rowOneLeftBS->Add(new wxStaticText(this, wxID_ANY, 
      enumToWxString(languages::LAB_NUMBER_OF_CYCLES),wxDefaultPosition,wxDefaultSize), 0, wxALIGN_CENTER | wxTOP,5);
  rowOneLeftBS -> Add(sliderCycle, 2, wxEXPAND, 0);


  rowOneBS -> Add(rowOneLeftBS,3,wxEXPAND);
  rowOneBS -> Add(btnRun, 1, wxEXPAND, 0);
  rowOneBS -> Add(btnReset, 1, wxEXPAND, 0);

  rowOneLeftBS->Layout();


  // ROW TWO: Traces ---------------------------------------------------

  rowTwoPointFiveBS->Add( new wxStaticText(this, wxID_ANY, 
      enumToWxString(languages::LAB_SWITCHES)), 1, wxEXPAND);

  slideTogetherCB = new wxCheckBox(this, -1, enumToWxString(languages::LAB_MONITORS_SLIDE_TOGETHER));
  rowTwoPointFiveBS->Add( slideTogetherCB, 1, wxEXPAND );

  // ROW THREE: Switches checklist + New monitor ------------------------
  rowThreeBS -> Add(switchList,1,wxEXPAND);
  rowThreeBS -> Add(btnMonitorAdd,1,wxEXPAND);


  // ROW FOUR: Console
  rowFourBS->Add(txtboxConsole, 1 , wxEXPAND);

  //add the miniboxes to the big box
  mainBoxSizer->Add(rowOneBS, 1, wxEXPAND);
  mainBoxSizer->Add(rowTwoBS, 4, wxEXPAND);
  mainBoxSizer->Add(rowTwoPointFiveBS, 0, wxEXPAND);

  mainBoxSizer->Add(rowThreeBS, 1, wxEXPAND);
  mainBoxSizer->Add(-1, 10);
  mainBoxSizer->Add(rowFourBS, 1, wxEXPAND);




  //hbox->Add(mainBoxSizer, 1, wxALL | wxEXPAND, 15);
  SetBackgroundColour(*wxWHITE);
  SetSizer(mainBoxSizer); //need this!
  Centre();

  populateSwitches();
  populateMonitors();
  addInitialErrors();

  //Initialise the random number generator
  srand (time(NULL));

  cyclescompleted = 0;
}

void MyFrame::populateMonitors(){
  int the_moncount =  mmz->moncount();
  //RESET ALL
  for (unsigned int i=0;i<arrCanvas.size();i++){
    arrCanvas[i]->Destroy();
  }


  for (unsigned int i=0;i<arrDeleteButtons.size();i++){
    arrDeleteButtons[i]->Destroy();
  }

  for (unsigned int i=0;i<arrMonLabels.size();i++){
    arrMonLabels[i]->Destroy();
  }

  arrCanvas.clear();
  arrPaneBoxSizers.clear();
  arrDeleteButtons.clear();
  arrMonLabels.clear();

  rowTwoBS->Clear();

  for(int i=0;i<the_moncount;i++)
  {
    name dev;
    name outp;
    mmz->getmonname(i, dev, outp);

    namestring devns = nameToNamestring(dev);
    namestring outpns = nameToNamestring(outp);
    if (outpns == "blankname"){
      outpns = "";
    } else {
      devns += "\n";
    }

    //new box sizer,
    arrPaneBoxSizers.push_back(new wxBoxSizer(wxHORIZONTAL));

    //new text
    wxString devwxs = wxString::FromAscii(devns.c_str());
    wxString outpwxs = wxString::FromAscii(outpns.c_str());
    wxString monname = devwxs+ outpwxs;
    arrMonLabels.push_back(new wxStaticText(this, -1, monname));
    arrPaneBoxSizers[i]->Add(arrMonLabels[i],1, wxALIGN_CENTER_VERTICAL | wxALIGN_CENTER_HORIZONTAL);

    //new canvas + add it
    arrCanvas.push_back(new MyGLCanvas(this,
        wxID_ANY,
        mmz,
        nmz,
        wxPoint(0,0),
        wxSize(100,30)
    ));
    arrCanvas[i]->setMonitorNum(i);
    arrPaneBoxSizers[i]->Add(arrCanvas[i],7, wxEXPAND);
    arrCanvas[i]->SetBackgroundColour(*wxBLACK);


    //new button
    arrDeleteButtons.push_back(new wxButton(this,BUTTON_MONITOR_REMOVE+i,enumToWxString(languages::LAB_REMOVE)));
    arrPaneBoxSizers[i]->Add(arrDeleteButtons[i],1,wxEXPAND | wxALIGN_RIGHT);

    rowTwoBS->Add(arrPaneBoxSizers[i],1,wxEXPAND);

    //a bit of space
    rowTwoBS->AddSpacer(10);
  }

  SetMinSize(wxSize( 420, the_moncount*60 + 260 ) ); 
  rowTwoBS->Layout();
  mainBoxSizer->Layout();
}

void MyFrame::populateSwitches(){
  wxLogMessage(wxString::FromUTF8(languages::langs[current_language][languages::MSG_INIT])); //to-do
  int numofswitches = dmz->getNumOfSwitches();
  for(int i = 0; i<numofswitches;i++){
    name nameID = dmz->getSwitchNameID(i);
    namestring switchname = nmz->at(nameID);
    switchList->Append(wxString::FromAscii(switchname.c_str()));

    //if its on, check it.
    if (dmz->getSwitchState(i) == HIGH){
      switchList->Check(i);
    }
  }
}



// Callback for the exit menu item
void MyFrame::OnExit(wxCommandEvent& /*event*/){
  Close(true);
}

// Callback for the about menu item
void MyFrame::OnAbout(wxCommandEvent& /* event */){
  wxMessageDialog about(this, enumToWxString(languages::ABOUT_TEXT),
      enumToWxString(languages::ABOUT_TITLE), wxICON_INFORMATION | wxOK);
  about.ShowModal();
}

// Callback for the run button
void MyFrame::OnRunButton(wxCommandEvent& /*event*/){
  if( errorLog::getInstance().size()!= 0 ){
    wxLogMessage(enumToWxString(languages::MSG_CANNOT_RUN_CAUSE_ERRORS));
    return;	
  }
  runnetwork(sliderCycle->GetValue());
}

// Callback for the push button
void MyFrame::OnResetButton(wxCommandEvent& /*event*/){
  dmz->resetDevices();	
  mmz->resetmonitor ();
  populateMonitors();
  runnetwork(0);
  cyclescompleted = 0;
  for( unsigned int i = 0; i < arrCanvas.size(); i++ ){
    arrCanvas[i]->Render(0);
  }
  wxStreamToTextRedirector redirect(txtboxConsole);
}

void MyFrame::OnSwitchChange(wxCommandEvent &event){

  int swnum = event.GetInt();
  //the number corresponds to the ith occurence of switch, and we have a function to get that!

  name swID = dmz->getSwitchNameID(swnum);

  if (switchList->IsChecked(swnum)){
    dmz->setswitch(swID, HIGH, cmdok);
  } else {
    dmz->setswitch(swID, LOW, cmdok);
  }
}

void MyFrame::OnSliderChange(wxScrollEvent& /*event*/){
}


void MyFrame::OnAddMonitorButton(wxCommandEvent& /*event*/){
  network* networkp = mmz->netz;
  devlink devicelist = networkp->devicelist();

  wxArrayString choices;
  vector<name> devicechoices;
  vector<name> outputchoices;

  for( ; devicelist != NULL; devicelist = devicelist->next ){
    namestring devicename = nameToNamestring( devicelist->id ); 	
    for( outplink outputs = devicelist->olist;
        outputs != NULL;  outputs = outputs->next ){
      if( outputs->id == names::BLANKNAME ){
        choices.Add( wxString::FromUTF8( devicename.c_str() ) );	
      } else {
        stringstream choice;
        choice << devicename << "." << nameToNamestring( outputs->id );
        choices.Add( wxString::FromUTF8( choice.str().c_str() ) );	 
      }
      devicechoices.push_back( devicelist->id );
      outputchoices.push_back( outputs->id );
    }
  } 

  int i = wxGetSingleChoiceIndex( enumToWxString(languages::NEWMON_SELECT_OUTPUT), 
      enumToWxString(languages::NEWMON_MON_POINT),choices );

  bool cmdok;
  mmz->makemonitor (devicechoices[i], outputchoices[i], cmdok);
  if (cmdok){
  } else {
    if( mmz->moncount() < MAXMONITORS ){
      wxLogMessage(enumToWxString(languages::MSG_MONITOR_POINT_NOT_EXIST));
    } else {
      wxLogMessage(enumToWxString(languages::MSG_MONITOR_LIMIT));
    }
  }
  int cycles = arrCanvas[0]->cyclesdisplayed;

  populateMonitors(); //redraw.
  for( unsigned int i = 0; i < arrCanvas.size(); i++ ){
    arrCanvas[i]->cyclesdisplayed = cycles;
  }  
  runnetwork(0);
}

void MyFrame::OnRemoveMonitorButton(wxCommandEvent &event){
  if (event.GetId()<BUTTON_MONITOR_REMOVE || event.GetId()>BUTTON_MONITOR_REMOVE + 9){
    //so max 10
    return;
  }
  int monnum = event.GetId() - BUTTON_MONITOR_REMOVE; //so gives 0 to 9

  name outp;
  name dev;

  int cycles = arrCanvas[0]->cyclesdisplayed;

  mmz->getmonname(monnum,dev,outp);
  mmz->remmonitor (dev, outp,cmdok);
  if (!cmdok) wxLogMessage(enumToWxString(languages::MSG_MONITOR_ERROR_REMOVE));
  populateMonitors(); //redraw the new tables.
  for( unsigned int i = 0; i < arrCanvas.size(); i++ ){
    arrCanvas[i]->cyclesdisplayed = cycles;
  }  
}


void MyFrame::updateGLSliders( int pos ){
  if( slideTogetherCB->IsChecked() ){
    for( unsigned int i = 0; i < arrCanvas.size(); i++ ){
      arrCanvas[i]->setSlider( pos );
    }  
  }

}

// Function to run the network, derived from corresponding function in userint.cc
void MyFrame::runnetwork(int ncycles){
  bool ok = true;
  int n = ncycles;

  while ((n > 0) && ok) {
    dmz->executedevices (ok);
    if (ok) {
      n--;
      mmz->recordsignals ();
    } else
      wxLogMessage(enumToWxString(languages::MSG_NETWORK_OSCILLATING));
  }
  if (ok){
    cyclescompleted = cyclescompleted + ncycles;
  } else {
    cyclescompleted = 0;
  }
  if( cyclescompleted > MAXCYCLES ){
    cyclescompleted = MAXCYCLES;
    wxLogMessage(enumToWxString(languages::MSG_CYCLE_LIMIT)) ;
  }
  int moncount = mmz->moncount();

  for (int i=0; i<moncount; i++)
  {
    arrCanvas[i]->Render(cyclescompleted);
    arrCanvas[i]->Refresh();
    arrCanvas[i]->Update();
    arrCanvas[i]->hasBeenDrawn = true;
  }
}

//gives name of namestring, catching errors.
name MyFrame::namestringToName(namestring ns){
  name n = nmz->cvtname (ns);
  if (n == names::BLANKNAME) {
    wxLogMessage(enumToWxString(languages::MSG_UNKNOWN_NAME));
    cmdok = false;
  }
  return n;
}

//gives name of namestring, catching errors.
namestring MyFrame::nameToNamestring(name n){
  if (n==names::BLANKNAME || n < 0 || n >= (int)nmz->size() ){
    return "blankname";
  }
  return nmz->at(n);
}

//catching errors.
void MyFrame::addInitialErrors(){
  errorLog& log = errorLog::getInstance();

  for( errorLog::iterator it = log.begin(); it != log.end(); it++ ){
    wxString rs = wxString::FromAscii((*it).getReasonString(current_language).c_str());
    wxString ls = wxString::FromAscii((*it).getLineString().c_str());
    wxLogMessage(rs);
    wxLogMessage(ls);
  }
  if( log.size()!= 0 ){
    wxLogMessage(enumToWxString(languages::MSG_ERROR_COUNT),log.size() );
  }
}

wxString MyFrame::enumToWxString (languages::messages the_enum){
  return wxString::FromUTF8(languages::langs[current_language][the_enum]);
}
