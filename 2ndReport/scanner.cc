#include <limits>
#include <iostream>
#include <cstdlib>
#include <cctype>
#include "scanner.h"


const char* scanner::NAME_INITIAL_CHARS =
 "_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; 

const char* scanner::NAME_CHARS =
 "_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; 

scanner::scanner( names *aNametable, const char *aFileName ):
	theNametable( aNametable ), theFileName( aFileName ){
}

scanner::scanner( names *aNametable, string aFileName ):
	theNametable( aNametable ), theFileName( aFileName ){
}

scanner::scanner(void){
}

scanner::~scanner(){
	for ( vector<symbol *>::iterator it = symbolStore.begin();
	    it!=symbolStore.end(); ++it){
		delete *it;
	}
}

void scanner::checkFileIsOpen(){
	if( false == theFile.is_open() ){
		theFile.open( theFileName.c_str() );
	} 
}

/* This is a bit Rube Goldberg esque
 * this is so that it ignored comments,
 * in order that if we only use this, comments should be handled automagically
 */
char scanner::getNextChar(){
	char current, next;
	theFile.get( current );

	if( theFile.eof() || current != '/' ){
		return current;
	} 
	theFile.get( next );

	if( theFile.eof() || ( next != '/' && next != '*' ) ){
		theFile.unget();
		return current;
	}  
	if( next == '/' ){ // we have a c++ style comment 	
		theFile.ignore( numeric_limits<streamsize>::max(), '\n' );
	} else if( next == '*' ){ // we have a c style comment
		while( next != '/' && theFile.eof() == false ){
			theFile.ignore( numeric_limits<streamsize>::max() , '*' );
			theFile.get( next );
		}
	}
	return getNextChar();
}


symbol& scanner::addSymbolToStore( symbol* s ){
	symbolStore.push_back( s );
	return *symbolStore.back();
}

symbol& scanner::skipUntilTerminator(){

	symbol &s = getNextSymbol();

	if( s.getType() == symbol::END_OF_FILE ){
		return s;
	} 

	if( s.getType() == symbol::PUNCTUATION ){
		punctuationSymbol *ps = static_cast<punctuationSymbol *>(&s);
		if( ps->getPunctuationType() == punctuationSymbol::CLOSE_BRACE ){
			return s;
		}
		if( ps->getPunctuationType() == punctuationSymbol::SEMICOLON ){
			return s;
		}
	}
	
    return skipUntilTerminator();
}

void scanner::skipUntilBraceClosed(){

	symbol &s = getNextSymbol();

	if( s.getType() == symbol::END_OF_FILE ){
		return;
	} 

	if( s.getType() == symbol::PUNCTUATION ){
		punctuationSymbol *ps = static_cast<punctuationSymbol *>(&s);
		if( ps->getPunctuationType() == punctuationSymbol::OPEN_BRACE ){
			//* TODO, throw an exception here
		}
		if( ps->getPunctuationType() == punctuationSymbol::CLOSE_BRACE ){
			return;
		}
	}
	
    skipUntilBraceClosed();
}

symbol& scanner::getNextDeviceName(){

	checkFileIsOpen();
	
	symbol &s = getNextSymbol();
	if( s.getType() == symbol::NAME || 
	    s.getType() == symbol::KEYWORD || 
		s.getType() == symbol::END_OF_FILE ){
		return s;
	} 
	if( s.getType() == symbol::PUNCTUATION ){
		punctuationSymbol *ps = static_cast<punctuationSymbol *>(&s);
		if( ps->getPunctuationType() == punctuationSymbol::OPEN_BRACE ){
			skipUntilBraceClosed();
		}

	}
	
	return getNextDeviceName();

}

void scanner::rewind(){
	/* we need to clear the eof/error flags before we can seek */
	theFile.clear();
	theFile.seekg( 0, theFile.beg );
}

void scanner::skipWhitespace(){
	char c;
	
	while( !theFile.eof() && isspace( c = getNextChar() ) ){
	}
	if( !isspace( c ) ){
		theFile.unget();
	}
}

symbol& scanner::genNameSymbol(){
	char c;
	namestring ns;
	while( !theFile.eof() &&
		string( NAME_CHARS ).find_first_of( c = getNextChar() )
			!= string::npos ){
		ns.push_back( c );
	} 
	theFile.unget();

	for( int i = 0; i< keywordSymbol::NO_KEYWORDS ; i++ ){
		if( ns.compare( string( keywordSymbol::KEYWORDS[i] ) ) == 0 ){
			return addSymbolToStore(
				new keywordSymbol( (keywordSymbol::keywordType) i )
			);
		}
	} 

	return addSymbolToStore( new nameSymbol( theNametable->lookup( ns ) ) );
}

symbol& scanner::genValueSymbol(){
	char c; 
	int v = 0;
	while( !theFile.eof() && isdigit( c = getNextChar() ) ){ 
		v *= 10;
		v += c -'0'; 
	} 
	theFile.unget();
	return addSymbolToStore( new valueSymbol( v ) );
}


symbol& scanner::genPunctuationSymbol( char c ){
	punctuationSymbol::punctuationType p = punctuationSymbol::INVALID;

	switch( c ){ 
		case '.':
			p = punctuationSymbol::STOP;
			break;
		case '=':
			p = punctuationSymbol::EQUALS;
			break;
		case ';':
			p = punctuationSymbol::SEMICOLON;
			break;
		case '{':
			p = punctuationSymbol::OPEN_BRACE;
			break;
		case '}':
			p = punctuationSymbol::CLOSE_BRACE;
			break;
		default:
			p = punctuationSymbol::INVALID;
			break;
			//TODO: return error if invalid punctuation is found 
	}
	return addSymbolToStore( new punctuationSymbol( p ) );
}

symbol& scanner::getNextSymbol(){
	checkFileIsOpen();

	skipWhitespace();
	if( theFile.eof() || !theFile.good() ){
		return addSymbolToStore( new eofSymbol() );
	}

	char c = getNextChar();

	if ( string( NAME_INITIAL_CHARS ).find_first_of( c ) != string::npos ){
		theFile.unget();
		return genNameSymbol();
	} 
	if( isdigit( c ) ){
		theFile.unget();
		return genValueSymbol();
	}
	return genPunctuationSymbol( c );
}

pair<int, int> scanner::getPositionInFile(){

	/* get the position in bytes */
	int position = theFile.tellg();

	/* rewind to the start of the file */
	rewind(); 


	/* then count the number of lines and characters */
	int linenumber = 1;
	int charnumber = 0;

	char c;
	for( int i = 0; i< position && theFile.eof() ==false; i++ ){
		theFile.get( c );
		charnumber++;
		if( c == '\n' ){
			linenumber++;
			charnumber = 0;
		}
	}
	return pair<int, int>( linenumber, charnumber );
}

string scanner::getCurrentLine(){

	/* get the position in bytes */
	int position = theFile.tellg();

	pair<int, int> linepos = getPositionInFile();

	theFile.seekg( -linepos.second, theFile.cur );

	char buffer[256];
	theFile.getline( buffer, 256 );
	
	/* restore the position in the file */
	theFile.clear();
	theFile.seekg(position, theFile.beg);

	return string( buffer);
}

