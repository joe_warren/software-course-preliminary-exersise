#ifndef symbol_h
#define symbol_h

#include "names.h"

class symbol {
public:
  enum symbolType{
    NAME,
    KEYWORD,
    VALUE,
    END_OF_FILE,
    PUNCTUATION
  };
  symbolType getType();
protected:
  /* default constructor, not utilised */
  symbol(void);
  symbolType type;
};

class nameSymbol: public symbol {
public:
  nameSymbol( name aName );
  name getName();
private:
  name theName;
};


class valueSymbol: public symbol {
public:
  valueSymbol( int aValue );
  int getValue();
private:
  int theValue;
};

class eofSymbol: public symbol {
public:
  eofSymbol();
}; 

class punctuationSymbol: public symbol {
public:
  enum punctuationType{
    INVALID,
    STOP,
    EQUALS,
    SEMICOLON,
    OPEN_BRACE,
    CLOSE_BRACE
  };
  punctuationSymbol( punctuationType aPunctuationType );
  punctuationType getPunctuationType();
private:
  punctuationType thePunctuationType;
};

class keywordSymbol: public symbol {
public:
  enum keywordType{
    CLOCK = 0,
    RCDEVICE,
    SWITCH,
    AND,
    NAND,
    OR,
    NOR,
    DTYPE,
    XOR,
    MONITOR,
    INITIAL,
    TIMECONSTANT,
    PERIOD
  };
  static const int NO_KEYWORDS = 13;
  static const char* KEYWORDS[];

  keywordSymbol( keywordType aKeywordType );
  keywordType getKeywordType();
private:
  keywordType theKeywordType;
};


#endif /* symbol_h */
