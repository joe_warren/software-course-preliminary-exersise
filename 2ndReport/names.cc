#include "names.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <cstdlib>

using namespace std;


const int namestring::maxlength = 8;  /* max chars in a name string */

const int names::maxnames  = 200;  /* max number of distinct names */

const int names::blankname = -1;   /* special name */

/* construct a namestring from a string literal */
namestring::namestring( const char* charAr ):
	string( charAr ){
}

/* construct a namestring from a string object */
namestring::namestring( string str ):
	string( str ){
}

/* default constructor */
namestring::namestring(){
} 


/* Name storage and retrieval routines */

/* default constructor */
names::names(void){
}

name names::lookup (namestring str)
{
	name theName = cvtname( str );
	if( theName == blankname ){
		push_back( str );
		theName = cvtname( str );
  	}  
  	return theName;
}

name names::cvtname (namestring str)
{
	names::iterator p = find( begin(), end(), str );
	if( p == end() ){ 
		return blankname;
	} else {
		return distance( begin(), p ); 
	}
}

void names::printContents(){
	cout << "Name List Contains:" << endl;
	int i = 0;
	/* use an iterator to loop through the names */
	for ( names::iterator it=begin(); it!=end(); ++it, i++){
		cout << "\t" << i << " : " << *it << endl;
	}
}

void names::writename (name id)
{
	namestring string = at(id);
	cout << string;
}

int names::namelength (name id)
{
	return at(id).length();
}
































