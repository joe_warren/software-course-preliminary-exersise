#include <stdio.h>

#include "inputexception.h"
#include "gtest/gtest.h"

using namespace std;

namespace {


TEST(inputExceptionTest, ConstructorWorks ){
	
	inputException *ie = new inputException(
		inputException::INVALID_PROPERTY_NAME, 
		pair<int,int>( 2, 5 ),  "a line with an error" );

	ASSERT_NE( (inputException*)NULL, ie ); 
}


TEST(inputExceptionTest, getReasonStringWorks ){
	
	inputException ie( inputException::INVALID_PROPERTY_NAME, 
		pair<int,int>( 2, 5 ),  "a line with an error" );
	EXPECT_STREQ("An invalid property name was found", 
		ie.getReasonString().c_str() );
}

TEST(inputExceptionTest, getLineStringWorks ){
	
	inputException ie( inputException::INVALID_PROPERTY_NAME, 
		pair<int,int>( 2, 5 ),  "a line with an error" );

	EXPECT_STREQ("An error occured on line 2:\n"
	             "a line with an error\n"
                 "     ^\n", 
		ie.getLineString().c_str() );
}
};
