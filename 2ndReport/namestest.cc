
#include "names.h"
#include "gtest/gtest.h"

namespace {

class namesTest : public ::testing::Test {
 protected:

	namesTest() {
	}

	virtual ~namesTest() {
	}

	virtual void SetUp() {
		nameTable  = new names();
	}

	virtual void TearDown() {
      delete nameTable;
      index1 = names::blankname;
      index2 = names::blankname;
	}
	names * nameTable;

    name index1;
    name index2;

	virtual void addSomeNames(){

    	index1 = nameTable->lookup( "testname" ); 
    	index2 = nameTable->lookup( "another" ); 
	}
};

TEST_F(namesTest, CanAddANameUsingLookup ){

    nameTable->lookup( "testname" ); 
	EXPECT_STREQ("testname", nameTable->at(0).c_str() );
}

TEST_F(namesTest, CanLookupANameUsingLookup ){

    addSomeNames();

	EXPECT_EQ( 0, nameTable->lookup("testname") );
	EXPECT_EQ( 1, nameTable->lookup("another") );
}

TEST_F(namesTest, CanLookupANameUsingCvtname ){

    addSomeNames();

	EXPECT_EQ( 0, nameTable->cvtname("testname") );
	EXPECT_EQ( 1, nameTable->cvtname("another") );
}

TEST_F(namesTest, 
	UsingCvtnameDoesNotAddANonExistantNameButReturnsBlankName ){

    addSomeNames();

	EXPECT_EQ( names::blankname, nameTable->cvtname("notadded") );
}

TEST_F(namesTest, UsingLookupOnANameManyTimesOnlyAddsItOnce ){

    addSomeNames();
    addSomeNames();

	EXPECT_EQ( 2, nameTable->size() );
}

TEST_F(namesTest, NameLengthUsedOnAnIndexGetsTheLengthOfTheNameString ){

    addSomeNames();

	EXPECT_EQ( strlen("testname"), nameTable->namelength(index1) );
	EXPECT_EQ( strlen("another"), nameTable->namelength(index2) );
}


}
