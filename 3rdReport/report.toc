\contentsline {section}{\numberline {1}The Logic Simulator}{1}
\contentsline {subsection}{\numberline {1.1}The Software Structure}{1}
\contentsline {section}{\numberline {2}Testing Strategy}{2}
\contentsline {subsection}{\numberline {2.1}The Scanner}{2}
\contentsline {subsection}{\numberline {2.2}The Parser}{3}
\contentsline {subsection}{\numberline {2.3}The GUI}{3}
\contentsline {section}{\numberline {3}Teamwork}{3}
\contentsline {subsection}{\numberline {3.1}Source Code Control}{3}
\contentsline {subsection}{\numberline {3.2}Coding Standards}{4}
\contentsline {subsection}{\numberline {3.3}Deadlines}{4}
\contentsline {section}{\numberline {4}Area of Responsibility}{4}
\contentsline {section}{\numberline {5}Changes Made to the Supplied Source Code}{5}
\contentsline {section}{\numberline {6}Conclusion \& Areas for Improvement}{5}
\contentsline {section}{\numberline {A}Listings}{I}
\contentsline {subsection}{\numberline {A.1}Main Codebase}{I}
\contentsline {subsubsection}{\numberline {A.1.1}errorlog.h}{I}
\contentsline {subsubsection}{\numberline {A.1.2}errorlog.cc}{I}
\contentsline {subsubsection}{\numberline {A.1.3}gui.h}{II}
\contentsline {subsubsection}{\numberline {A.1.4}gui.cc}{III}
\contentsline {subsubsection}{\numberline {A.1.5}inputexception.h}{XII}
\contentsline {subsubsection}{\numberline {A.1.6}inputexception.cc}{XIII}
\contentsline {subsubsection}{\numberline {A.1.7}names.h}{XIV}
\contentsline {subsubsection}{\numberline {A.1.8}names.cc}{XV}
\contentsline {subsubsection}{\numberline {A.1.9}scanner.h}{XVI}
\contentsline {subsubsection}{\numberline {A.1.10}scanner.cc}{XVII}
\contentsline {subsubsection}{\numberline {A.1.11}symbol.h}{XXI}
\contentsline {subsubsection}{\numberline {A.1.12}symbol.cc}{XXII}
\contentsline {subsubsection}{\numberline {A.1.13}parser.h}{XXIII}
\contentsline {subsubsection}{\numberline {A.1.14}parser.cc}{XXIII}
\contentsline {subsubsection}{\numberline {A.1.15}Makefile}{XXIV}
\contentsline {subsection}{\numberline {A.2}Test Code}{XXV}
\contentsline {subsubsection}{\numberline {A.2.1}errorlogtest.cc}{XXV}
\contentsline {subsubsection}{\numberline {A.2.2}inputexceptiontest.cc}{XXV}
\contentsline {subsubsection}{\numberline {A.2.3}namestest.cc}{XXVI}
\contentsline {subsubsection}{\numberline {A.2.4}scannertest.cc}{XXVII}
\contentsline {subsection}{\numberline {A.3}Modified Legacy Code}{XXXIV}
\contentsline {subsubsection}{\numberline {A.3.1}devices.h}{XXXIV}
\contentsline {subsubsection}{\numberline {A.3.2}devices.cc}{XXXIV}
\contentsline {subsubsection}{\numberline {A.3.3}monitor.h}{XLI}
\contentsline {subsubsection}{\numberline {A.3.4}monitor.cc}{XLII}
\contentsline {subsubsection}{\numberline {A.3.5}network.h}{XLIV}
\contentsline {subsubsection}{\numberline {A.3.6}network.cc}{XLV}
\contentsline {section}{\numberline {B}Attached User Guide }{XLIX}
\contentsline {section}{\numberline {C}Example Definition Files}{L}
\contentsline {subsection}{\numberline {C.1}Example 1}{L}
\contentsline {subsection}{\numberline {C.2}Example 2}{LI}
\contentsline {section}{\numberline {D}File Format}{LII}
\contentsline {section}{\numberline {E}Files in the Team Directory}{LIII}
