#include <stdio.h>

#include "scanner.h"
#include "gtest/gtest.h"

namespace {

class scannerTest : public ::testing::Test {
 protected:
	scannerTest() {
	}

	virtual ~scannerTest() {
	}

	virtual void SetUp() {
		nameTable  = new names();
		remove( TMP_FILENAME );
	}
	virtual void TearDown() {
    	delete nameTable;
		remove( TMP_FILENAME );
	}

	virtual void createTestFileFromString( string contents ){
		ofstream file;
		file.open( TMP_FILENAME, ofstream::out );
		file << contents;
		file.close();
	}

	virtual namestring getNameStringFromSymbol( symbol& s ){
		if( s.getType() != symbol::NAME ){
			stringstream ss;
			ss << "NOT A NAME SYMBOL :: " << s.getType();
			return ss.str();
		}
		nameSymbol& ns = static_cast<nameSymbol&>( s );
		return nameTable->at( ns.getName() );
	}

	void checkNextDeviceNameSymbol( scanner* aScanner, const char *expected  ){
		EXPECT_STREQ(expected, 
		             getNameStringFromSymbol(aScanner->getNextDeviceName())
		               .c_str()
		            );
	} 

	virtual int getIntFromSymbol( symbol& s ){
		if( s.getType() != symbol::VALUE ){
			return -1;
		}
		valueSymbol& vs = static_cast<valueSymbol&>( s );
		return vs.getValue();
	}

	void checkNextNameSymbol( scanner* aScanner, const char *expected  ){
		EXPECT_STREQ(expected, 
		             getNameStringFromSymbol(aScanner->getNextSymbol())
		               .c_str()
		);
	} 

	void checkNextIntSymbol( scanner* aScanner,  int expected  ){
		EXPECT_EQ(expected, 
		             getIntFromSymbol(aScanner->getNextSymbol())
		);
	} 

	virtual punctuationSymbol::punctuationType
		getPunctuationFromSymbol( symbol& s ){

		if( s.getType() != symbol::PUNCTUATION ){
			return punctuationSymbol::INVALID;
		}
		punctuationSymbol& ps = static_cast<punctuationSymbol&>( s );
		return ps.getPunctuationType();
	}


	virtual punctuationSymbol::punctuationType
		makePunctuationType( char c ){
		switch( c ){ 
			case '.':
				return punctuationSymbol::STOP;
			case '=':
				return punctuationSymbol::EQUALS;
			case ';':
				return punctuationSymbol::SEMICOLON;
			case '{':
				return punctuationSymbol::OPEN_BRACE;
			case '}':
				return punctuationSymbol::CLOSE_BRACE;
			default:
				return punctuationSymbol::INVALID;
		}
	}
	void checkNextPunctuationSymbol( scanner* aScanner,  char expected  ){
		EXPECT_EQ(makePunctuationType( expected ), 
		             getPunctuationFromSymbol(aScanner->getNextSymbol())
		);
	} 

	virtual keywordSymbol::keywordType
		getKeywordFromSymbol( symbol& s ){

		if( s.getType() != symbol::KEYWORD ){
			return (keywordSymbol::keywordType)-1;
		}
		keywordSymbol& ps = static_cast<keywordSymbol&>( s );
		return ps.getKeywordType();
	}

	void checkNextKeywordSymbol( scanner* aScanner,  
		keywordSymbol::keywordType expected  ){
		EXPECT_EQ( expected , 
			getKeywordFromSymbol(aScanner->getNextSymbol())
		);
	} 

	void checkNextDeviceKeywordSymbol( scanner* aScanner,  
		keywordSymbol::keywordType expected  ){
		EXPECT_EQ( expected , 
			getKeywordFromSymbol(aScanner->getNextDeviceName())
		);
	} 

	void checkStandardDeviceNameSymbols( scanner* aScanner ){


 		checkNextDeviceKeywordSymbol( aScanner, keywordSymbol::CLOCK );
 		checkNextDeviceNameSymbol( aScanner, "C1" );
 		checkNextDeviceKeywordSymbol( aScanner, keywordSymbol::CLOCK );
 		checkNextDeviceNameSymbol( aScanner, "C2" );
 		checkNextDeviceKeywordSymbol( aScanner, keywordSymbol::SWITCH );
 		checkNextDeviceNameSymbol( aScanner, "S" );
 		checkNextDeviceKeywordSymbol( aScanner, keywordSymbol::AND );
 		checkNextDeviceNameSymbol( aScanner, "G1" );
 		checkNextDeviceKeywordSymbol( aScanner, keywordSymbol::NOR );
 		checkNextDeviceNameSymbol( aScanner, "G2" );
 		checkNextDeviceKeywordSymbol( aScanner, keywordSymbol::XOR );
 		checkNextDeviceNameSymbol( aScanner, "G3" );
 		checkNextDeviceKeywordSymbol( aScanner, keywordSymbol::OR );
 		checkNextDeviceNameSymbol( aScanner, "G4" );
 		checkNextDeviceKeywordSymbol( aScanner, keywordSymbol::MONITOR );
 		checkNextDeviceNameSymbol( aScanner, "monitor" );

		EXPECT_EQ(symbol::END_OF_FILE, aScanner->getNextDeviceName().getType());
	
	}


	void checkClockSymbols( scanner* aScanner, int period, const char* name ){
 		checkNextKeywordSymbol( aScanner, keywordSymbol::CLOCK );
 		checkNextPunctuationSymbol( aScanner, '{' );
 		checkNextKeywordSymbol( aScanner, keywordSymbol::PERIOD );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextIntSymbol( aScanner, period );
 		checkNextPunctuationSymbol( aScanner, ';' );
 		checkNextPunctuationSymbol( aScanner, '}' );
 		checkNextNameSymbol( aScanner, name );
 		checkNextPunctuationSymbol( aScanner, ';' );

	}
	void checkStandardSymbols( scanner* aScanner ){

		/* clocks */
		checkClockSymbols( aScanner, 5, "C1" );
		checkClockSymbols( aScanner, 9, "C2" );

		/* Switch */
 		checkNextKeywordSymbol( aScanner, keywordSymbol::SWITCH );
 		checkNextPunctuationSymbol( aScanner, '{' );
 		checkNextKeywordSymbol( aScanner, keywordSymbol::INITIAL );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextIntSymbol( aScanner, 1 );
 		checkNextPunctuationSymbol( aScanner, ';' );
 		checkNextPunctuationSymbol( aScanner, '}' );
 		checkNextNameSymbol( aScanner, "S" );
 		checkNextPunctuationSymbol( aScanner, ';' );

		/* And Gate */
 		checkNextKeywordSymbol( aScanner, keywordSymbol::AND );
 		checkNextPunctuationSymbol( aScanner, '{' );

 		checkNextNameSymbol( aScanner, "I1" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "S" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextNameSymbol( aScanner, "I2" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "C1" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextPunctuationSymbol( aScanner, '}' );
 		checkNextNameSymbol( aScanner, "G1" );
 		checkNextPunctuationSymbol( aScanner, ';' );

		/* NOR GATE */
 		checkNextKeywordSymbol( aScanner, keywordSymbol::NOR );
 		checkNextPunctuationSymbol( aScanner, '{' );

 		checkNextNameSymbol( aScanner, "I1" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "G1" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextNameSymbol( aScanner, "I2" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "S" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextNameSymbol( aScanner, "I3" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "C2" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextPunctuationSymbol( aScanner, '}' );
 		checkNextNameSymbol( aScanner, "G2" );
 		checkNextPunctuationSymbol( aScanner, ';' );

		/* XOR Gate */
		checkNextKeywordSymbol( aScanner, keywordSymbol::XOR );
 		checkNextPunctuationSymbol( aScanner, '{' );

 		checkNextNameSymbol( aScanner, "I1" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "C1" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextNameSymbol( aScanner, "I2" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "G2" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextPunctuationSymbol( aScanner, '}' );
 		checkNextNameSymbol( aScanner, "G3" );
 		checkNextPunctuationSymbol( aScanner, ';' );

		/* OR GATE */
 		checkNextKeywordSymbol( aScanner, keywordSymbol::OR );
 		checkNextPunctuationSymbol( aScanner, '{' );

 		checkNextNameSymbol( aScanner, "I1" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "C1" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextNameSymbol( aScanner, "I2" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "G1" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextNameSymbol( aScanner, "I3" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "G3" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextPunctuationSymbol( aScanner, '}' );
 		checkNextNameSymbol( aScanner, "G4" );
 		checkNextPunctuationSymbol( aScanner, ';' );

		/* Monitor */
 		checkNextKeywordSymbol( aScanner, keywordSymbol::MONITOR );
 		checkNextPunctuationSymbol( aScanner, '{' );

 		checkNextNameSymbol( aScanner, "I" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "C1" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextNameSymbol( aScanner, "I" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "C2" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextNameSymbol( aScanner, "I" );
 		checkNextPunctuationSymbol( aScanner, '=' );
 		checkNextNameSymbol( aScanner, "G4" );
		checkNextPunctuationSymbol( aScanner, ';' );

 		checkNextPunctuationSymbol( aScanner, '}' );
 		checkNextNameSymbol( aScanner, "monitor" );
 		checkNextPunctuationSymbol( aScanner, ';' );

		EXPECT_EQ(symbol::END_OF_FILE, aScanner->getNextSymbol().getType());
	
	}

	static const char * TMP_FILENAME; 

	static const char * SAMPLE_FILE;

	static const char * COMMENT_FILE;

	names * nameTable;


};

const char * scannerTest::TMP_FILENAME = "/tmp/scannertestfile.txt";

const char * scannerTest::SAMPLE_FILE =
"CLOCK {\n"
"period = 5;\n"
"} C1;\n"
"CLOCK {\n"
"period = 9;\n"
"} C2;\n"
"SWITCH {\n"
"initial = 1;\n"
"} S;\n"
"AND {\n"
"I1 = S;\n"
"I2 = C1;\n"
"} G1;\n"
"NOR {\n"
"I1 = G1;\n"
"I2 = S;\n"
"I3 = C2;\n"
"} G2;\n"
"XOR {\n"
"I1 = C1;\n"
"I2 = G2;\n"
"} G3;\n"
"OR{\n"
"I1 = C1;\n"
"I2 = G1;\n"
"I3 = G3;\n"
"} G4;\n"
"MONITOR{\n"
"I = C1;\n"
"I = C2;\n"
"I = G4;\n"
"} monitor;\n";

const char * scannerTest::COMMENT_FILE =
"CLOCK {\n"
"// A WHOLE LINE C++ STYLE COMMENT\n"
"/* A MULTILINE C STYLE COMMENT\n"
" */\n"
"/* A PREFIX COMMENT */ period = 5;\n"
"} C1;\n"
"CLOCK { /* A SUFFIX C STYLE COMMENT */\n"
"period = 9;//A SUFFIX C++ STYLE COMMENT\n"
"} C2;\n"
"SWI/* A COMMENT IN THE MIDDLE OF A NAME */TCH {\n"
"initial = 1;\n"
"} S;\n"
"AND {\n"
"I1 = S;\n"
"I2 = C1;\n"
"} G1;\n"
"NOR {\n"
"I1 = G1;\n"
"I2 = S;\n"
"I3 = C2;\n"
"} G2;\n"
"XOR {\n"
"I1 = C1;\n"
"I2 = G2;\n"
"} G3;\n"
"OR{\n"
"I1 = C1;\n"
"I2 = G1;\n"
"I3 = G3;\n"
"} G4;\n"
"MONITOR{\n"
"I = C1;\n"
"I = C2;\n"
"I = G4;\n"
"} monitor;\n";



TEST_F(scannerTest, ScannerCanBeConstructedFromANameTableAndFilePath ){
	
	scanner *aScanner = new scanner( nameTable, "file.cfg" );
	// cast required to remove compiler warning 
	//   regarding comparison between pointer and integer
	ASSERT_NE((scanner*)NULL, aScanner);
	delete aScanner;
}

TEST_F(scannerTest, ScannerWithAnNonExistentFileProducesEOFSymbol ){
	
	scanner *aScanner = new scanner( nameTable, "/tmp/thisdoesntexist" );

	EXPECT_EQ(symbol::END_OF_FILE, aScanner->getNextSymbol().getType());

	delete aScanner;
}

TEST_F(scannerTest, getNameSymbolReturnsDeviceSymbolNamesOnly ){
	
	createTestFileFromString( SAMPLE_FILE );

	scanner *aScanner = new scanner( nameTable, TMP_FILENAME );

	checkStandardDeviceNameSymbols( aScanner );

	delete aScanner;
}


TEST_F(scannerTest, scannerCanBeRewoundToEnableMultipassScanning ){
	
	createTestFileFromString( SAMPLE_FILE );

	scanner *aScanner = new scanner( nameTable, TMP_FILENAME );

	checkStandardDeviceNameSymbols( aScanner );
	aScanner->rewind();
	checkStandardDeviceNameSymbols( aScanner );

	delete aScanner;
}

TEST_F(scannerTest, getSymbolReturnsAllTheSymbols ){
	
	createTestFileFromString( SAMPLE_FILE );

	scanner *aScanner = new scanner( nameTable, TMP_FILENAME );

	checkStandardSymbols( aScanner );

	delete aScanner;
}

TEST_F(scannerTest, multipassParsingWorks ){
	
	createTestFileFromString( SAMPLE_FILE );

	scanner *aScanner = new scanner( nameTable, TMP_FILENAME );

	checkStandardDeviceNameSymbols( aScanner );
	aScanner->rewind();
	checkStandardSymbols( aScanner );

	delete aScanner;
}

TEST_F(scannerTest, commentsDoNotInteruptParsing ){
	
	createTestFileFromString( COMMENT_FILE );

	scanner *aScanner = new scanner( nameTable, TMP_FILENAME );

	checkStandardSymbols( aScanner );

	delete aScanner;
}


TEST_F(scannerTest, getPositionInFileWorks ){
	
	createTestFileFromString( SAMPLE_FILE );

	scanner *aScanner = new scanner( nameTable, TMP_FILENAME );

	pair<int, int> p;
	p = aScanner->getPositionInFile();
	EXPECT_EQ( 1, p.first );
	EXPECT_EQ( 0, p.second );

	/* clocks */
	checkClockSymbols( aScanner, 5, "C1" );
	p = aScanner->getPositionInFile();
	EXPECT_EQ( 3, p.first );
	EXPECT_EQ( 5, p.second );

	checkClockSymbols( aScanner, 9, "C2" );
	p = aScanner->getPositionInFile();
	EXPECT_EQ( 6, p.first );
	EXPECT_EQ( 5, p.second );


	/* Switch */
 	checkNextKeywordSymbol( aScanner, keywordSymbol::SWITCH );
 	checkNextPunctuationSymbol( aScanner, '{' );
 	checkNextKeywordSymbol( aScanner, keywordSymbol::INITIAL );

	p = aScanner->getPositionInFile();
	EXPECT_EQ( 8, p.first );
	EXPECT_EQ( 7, p.second );

 	checkNextPunctuationSymbol( aScanner, '=' );
 	checkNextIntSymbol( aScanner, 1 );
 	checkNextPunctuationSymbol( aScanner, ';' );

	p = aScanner->getPositionInFile();
	EXPECT_EQ( 8, p.first );
	EXPECT_EQ( 12, p.second );

 	checkNextPunctuationSymbol( aScanner, '}' );
 	checkNextNameSymbol( aScanner, "S" );
 	checkNextPunctuationSymbol( aScanner, ';' );

	p = aScanner->getPositionInFile();
	EXPECT_EQ( 9, p.first );
	EXPECT_EQ( 4, p.second );


	delete aScanner;
}

TEST_F(scannerTest, getCurrentLineWorks ){
	
	createTestFileFromString( SAMPLE_FILE );

	scanner *aScanner = new scanner( nameTable, TMP_FILENAME );

	pair<int, int> p;

	/* clocks */
	checkClockSymbols( aScanner, 5, "C1" );
	EXPECT_STREQ( "} C1;", aScanner->getCurrentLine().c_str() );

	checkClockSymbols( aScanner, 9, "C2" );
	EXPECT_STREQ( "} C2;", aScanner->getCurrentLine().c_str() );

	/* Switch */
 	checkNextKeywordSymbol( aScanner, keywordSymbol::SWITCH );

	EXPECT_STREQ( "SWITCH {", aScanner->getCurrentLine().c_str() );

 	checkNextPunctuationSymbol( aScanner, '{' );
 	checkNextKeywordSymbol( aScanner, keywordSymbol::INITIAL );

	EXPECT_STREQ( "initial = 1;", aScanner->getCurrentLine().c_str() );

 	checkNextPunctuationSymbol( aScanner, '=' );
 	checkNextIntSymbol( aScanner, 1 );
 	checkNextPunctuationSymbol( aScanner, ';' );

	EXPECT_STREQ( "initial = 1;", aScanner->getCurrentLine().c_str() );

 	checkNextPunctuationSymbol( aScanner, '}' );
 	checkNextNameSymbol( aScanner, "S" );
 	checkNextPunctuationSymbol( aScanner, ';' );

	EXPECT_STREQ( "} S;", aScanner->getCurrentLine().c_str() );

	delete aScanner;
}

TEST_F(scannerTest, skipCurrentTerminatorSkipsUpToASemiColonOrACloseBrace ){
	createTestFileFromString( SAMPLE_FILE );

	scanner *aScanner = new scanner( nameTable, TMP_FILENAME );

	/* clocks */
	checkClockSymbols( aScanner, 5, "C1" );

	checkClockSymbols( aScanner, 9, "C2" );

	/* Switch */
 	checkNextKeywordSymbol( aScanner, keywordSymbol::SWITCH );


 	checkNextPunctuationSymbol( aScanner, '{' );
 	checkNextKeywordSymbol( aScanner, keywordSymbol::INITIAL );

	EXPECT_EQ(makePunctuationType( ';' ), 
		             getPunctuationFromSymbol(aScanner->skipUntilTerminator())
		);

	EXPECT_EQ(makePunctuationType( '}' ), 
		             getPunctuationFromSymbol(aScanner->skipUntilTerminator())
		);


 	checkNextNameSymbol( aScanner, "S" );
 	checkNextPunctuationSymbol( aScanner, ';' );

 	checkNextKeywordSymbol( aScanner, keywordSymbol::AND );

	EXPECT_EQ(makePunctuationType( ';' ), 
		             getPunctuationFromSymbol(aScanner->skipUntilTerminator())
		);

	delete aScanner;
}

}



