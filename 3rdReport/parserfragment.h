 	bool isSpecificPunctuationType( symbol* aSymbol, 
		punctuationSymbol::punctuationType aType );

	bool isOpenBrace (symbol* aSymbol);
	bool isCloseBrace (symbol* aSymbol);
	bool isSemiColon (symbol* aSymbol);
	bool isStop (symbol* aSymbol);
	bool isEquals (symbol* aSymbol);
	bool isDeviceType (symbol* aSymbol);
	bool isPropertyName (symbol* aSymbol);
