#ifndef parser_h
#define parser_h

#include "names.h"
#include "scanner.h"
#include "network.h"
#include "monitor.h"
#include "devices.h"
#include "inputexception.h"
#include "errorlog.h"
#include "symbol.h"
#include <vector>
#include <algorithm>


using namespace std;

class parser {
private:
  network* netz; // instantiations of various classes for parser to use.
  devices* dmz; 
  monitor* mmz;
  scanner* smz;

  bool firstPass();
  bool secondPass();

  int getPeriod();
  int getInitial();
  int getTimeConstant();

  /*functions called by secondPass, as syntax named functions*/

  bool typeName(symbol* currentSymbol, devicekind& kind);
  bool input(symbol* currentSymbol, name devname, vector<name>& inputnames);
  bool monitorInput(symbol* currentSymbol);
  bool device(symbol* currentSymbol, vector<name>& devicenames);
  int countSemiColonsInBlock();

  /* Helper functions to discern between different symbol types */

  bool isSpecificPunctuationType( symbol* aSymbol,
      punctuationSymbol::punctuationType aType );

  bool isOpenBrace (symbol* aSymbol);
  bool isCloseBrace (symbol* aSymbol);
  bool isSemiColon (symbol* aSymbol);
  bool isStop (symbol* aSymbol);
  bool isEquals (symbol* aSymbol);
  bool isDeviceType (symbol* aSymbol);

  /* helper function to count inputs/parameters in current block */
  /* takes the scanner to the end of the current block */

public:
  bool readin ();
  /* Reads the definition of the logic system and builds the             */
  /* corresponding internal representation via calls to the 'Network'    */
  /* module and the 'Devices' module.                                    */

  parser (network* networkMod, devices* devicesMod,
      monitor* monitorMod, scanner* scannerMod);
  /* the constructor takes pointers to various other classes as parameters */
};

#endif /* parser_h */

