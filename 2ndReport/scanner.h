#ifndef scanner_h
#define scanner_h

#include <string>
#include <fstream>
#include "names.h"
#include "symbol.h"

class scanner {
	public:
		/* Construct scanner from a shared name table, 
		 * and a file to scan
		 * File opening is lazy
		 */
        scanner( names *aNametable, const char* file );
        scanner( names *aNametable, string file );

		/* free the symbol table */
		~scanner();

		/* used in first pass scanning  */
		symbol & getNextDeviceName();

		/* skip through symbols untill a close brace or a semicollon is found */
		symbol& skipUntilTerminator();

		/* rewind the file so that a second pass of scanning 
		 * can take place
		 */
		void rewind(void);

		/* used in second pass scanning */
		symbol& getNextSymbol();

		/* Used in error reporting, 
		 * pair.first = line number
		 * pair.second = character number
		 */
		pair<int, int> getPositionInFile();

		/* Used in error reporting 
		 */
		string getCurrentLine();
    private:
		/* Default Constructor: Not Used */
		scanner(void);

		/* performs lazy loading of the config file */ 
		void checkFileIsOpen();

		/* Get the next character from the file, ignoring comments */
		char getNextChar();

		/* remove whitespace chars from the filestream */
		void skipWhitespace();

		/* add a freshly 'new'd Symbol to the store, 
		 * then return a reference to it
		 */
		symbol& addSymbolToStore( symbol* s );

		/* skip through symbols untill a close brace is found */
		void skipUntilBraceClosed();

		/* parse a symbol out of the File and add  it to memory management */
		symbol& genNameSymbol();
		symbol& genValueSymbol();
		symbol& genPunctuationSymbol( char c );

	private:
		/* Store of local copies of each symbol,
		 * this allows us to return references to symbols 
		 * that don't expire when the original symbol goes out of scope
		 */
		vector<symbol *> symbolStore;
		names* theNametable;
		string theFileName;
		ifstream theFile;

		static const char* NAME_INITIAL_CHARS;
		static const char* NAME_CHARS;
				
};


#endif /* scanner_h */
