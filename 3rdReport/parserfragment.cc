bool parser::isSpecificPunctuationType( symbol* aSymbol, 
					punctuationSymbol::punctuationType aType ){

  if( aSymbol->getType() != symbol::PUNCTUATION ){
    return false;
  }
  punctuationSymbol *ps = static_cast<punctuationSymbol *> (aSymbol);
	
  if( ps->getPunctuationType() != aType ){
    return false;
  }
  return true;
}

bool parser::isOpenBrace (symbol* aSymbol){
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::OPEN_BRACE );
}
bool parser::isCloseBrace (symbol* aSymbol) {
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::CLOSE_BRACE );
}
bool parser::isSemiColon (symbol* aSymbol){
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::SEMICOLON );
}
bool parser::isStop (symbol* aSymbol) {
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::STOP );
}
bool parser::isEquals (symbol* aSymbol) {
  return isSpecificPunctuationType( aSymbol, punctuationSymbol::EQUALS );
}
bool parser::isDeviceType (symbol* aSymbol) {
  if( aSymbol->getType() != symbol::KEYWORD ){
    return false;
  }
  keywordSymbol *ks = static_cast<keywordSymbol *> (aSymbol);
	
  if( ks->getKeywordType() >= keywordSymbol::CLOCK && ks->getKeywordType() <= keywordSymbol::MONITOR ){
    return true;
  }
  return false;
}

bool parser::isPropertyName (symbol* aSymbol) {
  if( aSymbol->getType() != symbol::KEYWORD ){
    return false;
  }
  keywordSymbol *ks = static_cast<keywordSymbol *> (aSymbol);
	
  if( ks->getKeywordType() >= keywordSymbol::INITIAL && ks->getKeywordType() <= keywordSymbol::PERIOD ){
    return true;
  }
  return false;
}

