
\documentclass[a4paper,10pt]{article}

%\usepackage{a4wide}
\usepackage[cm]{fullpage}
\usepackage{amsmath}

\usepackage{array}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{listings}
\usepackage{fixltx2e}
\usepackage[justification=centering]{caption}
\usepackage{subcaption}
\usepackage{emp}
\usepackage{multirow}
\usepackage[miktex]{gnuplottex} 
\usepackage{epstopdf}
\usepackage{framed}
\usepackage{booktabs}

\DeclareGraphicsRule{*}{mps}{*}{}

\usepackage{color}
\usepackage[usenames,dvipsnames]{xcolor}
\definecolor{bggray}{rgb}{0.95,0.95,0.99}

\usepackage[runin]{abstract}

\usepackage{circuitikz}

\usetikzlibrary{shapes.misc}
\tikzset{cross/.style={cross out, draw, 
         minimum size=2*(#1-\pgflinewidth), 
         inner sep=0pt, outer sep=0pt}}

\usepackage{tikz}
\usepackage{gensymb}

\renewcommand{\abstractname}{Summary: } 
\renewcommand{\absnamepos}{empty}

\lstdefinelanguage{gtest}{ 
morecomment=[s]{[}{]},
}

\lstset{language=gtest,
               basicstyle=\color{Blue}\ttfamily\scriptsize,
               keywordstyle=\color{Green}\ttfamily,
               identifierstyle=\color{Blue}\ttfamily,
               stringstyle=\color{Fuchsia}\ttfamily,
               commentstyle=\color{Green}\ttfamily,
               backgroundcolor=\color{bggray},
               numberstyle=\footnotesize\color{Gray}\ttfamily,
               numbers=left,
               stepnumber=1,
               tabsize=4,	
               frame=leftline,
               rulecolor=\color{Gray},
	       numbersep=10pt,
               breaklines=true,
}

\lstset{language=C++,
               basicstyle=\color{Blue}\ttfamily\scriptsize,
               keywordstyle=\color{Cerulean}\ttfamily,
               identifierstyle=\color{Black}\ttfamily,
               stringstyle=\color{Fuchsia}\ttfamily,
               commentstyle=\color{Green}\ttfamily,
               backgroundcolor=\color{bggray},
               numberstyle=\footnotesize\color{Gray}\ttfamily,
               numbers=left,
               stepnumber=1,
               tabsize=4,	
               frame=leftline,
               rulecolor=\color{Gray},
	       numbersep=10pt,
               breaklines=true,
}

\newcommand{\indnt}{\-~~} 

\begin{document}

\pagenumbering{roman}

\title{ \textsc{GF2 :: Software\\Final Report} }
\author{\textsc{Joseph Warren} ( jw718 ) \\ \textsc{Churchill College}\\ \textsc{Team Zero}}
\date{30/5/14}

\maketitle

\begin{abstract}
This report contains an overview of a logic simulator that was developed in c++ by a team of three programmers. Development of the logic simulator took place over the course of several weeks.

The report contains an overview of a testing strategy. As well as a commentary on the approach taken to teamwork during development. A section describes the components of the application that each developer was responsible for.

An appendix contains source listings, the file format specification, example circuit files and a user guide describing the workflow of the software.
This user guide is written in a style style that mimics the documentation of a marketable product.
The appendix also includes a list of files produced in the development of the simulator.

This report contains listings of code developed during the scanner component task. 
\end{abstract}

\begin{figure}[H]
\centering
\includegraphics[width=0.10\textwidth]{logicicon.png}%
\end{figure} 


\vspace*{\fill}
{
\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{screenshot_small.png}%
\caption{A screen shot of the logic simulator} 
\end{figure} 
}
\vspace*{\fill}

\newpage

\tableofcontents

\newpage
\pagenumbering{arabic}

\section{The Logic Simulator}
The logic simulator operates by loading a file, then constructing a network of logic gates and other components from the network description within.

The program is then able to simulate the behaviour of the network.
A graphical user interface, or gui, displays the state of a selection of `monitor points' in the network, allowing the user to run the network for a number of steps, and displaying a trace showing the state of monitor points in the logic circuit over time. 
The simulator user interface contains the option to add and remove monitor points. 
The simulation can be reset, and check-boxes can  be used to toggle the state of switches in the network.

\subsection{The Software Structure}

Figure \ref{fig:classes} contains a very rough class diagram showing the relationship between the classes that make up the code of the logic simulator.

\begin{figure}[H]
\centering
\includegraphics{uml.1}
\caption{Class diagram showing the application structure}
\label{fig:classes}
\end{figure}

This diagram is neither complete, nor accurate.
For instance the gui class shown in the diagram actually consists of a class called \texttt{MyFrame} this controls a number of child classes with type \texttt{MyGLCanvas}. This UML class diagram is not intended to record that kind of detail; the diagram is intended to show the internals of the logic simulator at their highest level of organisation.

A rough overview of the relationship between the classes would be that an instance of the \texttt{logsim} class instantiates a single instance of a number of the classes, passing a filename into the \texttt{scanner}, and references to the required objects into several other classes.

 
The \texttt{logsim} class then defers to the \texttt{parser}, this in turn  calls into the \texttt{scanner}. The \texttt{scanner} returns \texttt{symbols}, \texttt{symbol} is an abstract class, with various concrete implementations, each representing a different pattern recognised by the \texttt{scanner} within the file.

The \texttt{parser} interprets the \texttt{symbols}, and populates the \texttt{network} and \texttt{monitor} classes accordingly. Errors are dealt with by throwing instances of the \texttt{inputException} class within the parser. \texttt{inputException}s are caught at a high level in the \texttt{parser}, and appended to a \texttt{errorLog}. \texttt{errorLog} is a singleton: only one \texttt{errorLog} can be instantiated per application, and any other attempts to create an \texttt{errorLog} will return it's lone instance. 

The gui classes display the information recorded by the \texttt{monitor}, and run the \texttt{network} when the run button is pressed. The gui also inspects the \texttt{errorLog}, and if it contains any exceptions, displays them. If \texttt{errorLog} contains exceptions, then the gui will refuse to run the \texttt{network}, as it may be in a bad state.

\section{Testing Strategy}

\subsection{The Scanner}
Within the scanner, small, individual code modules were tested using gtest: the Google testing framework.  
Listings of these unit tests are in Section \ref{sec:testing}.

Writing unit tests during development, allows bugs to be picked up as they are produced.
This style of programming is called test driven development. 
Using a test framework to automate tests makes it very convenient to run regression tests, that verify that a change to the software has not introduced bugs in previously functioning components. This was demonstrated by the \texttt{inputException} class, this had a large number of changes made when the localisation maintenance request was implemented.
A number of these changes introduced bugs, but since a decent sized set of tests existed for the \texttt{inputException} class, these bugs were detected as soon as they were produced.

Some sample test output is as follows.

\lstinputlisting[language=gtest]{testoutput.txt}

The \texttt{scanner} tests mostly operated by creating a temporary file with certain content, creating a \texttt{scanner} instance from this temporary file, and checking that the symbols produced by the \texttt{scanner} are as expected.

Using temporary files created by the test program, opposed to seperate test files, reduces the extent that the tests depend on the state of the file system, to much dependency on external state can lead to undesirable test behaviour. The temporary files are cleaned up when the tests finish.

\subsection{The Parser}

The parser was implemented under greater time pressure than the scanner, using poorer development methodologies. 
Parser tests were performed by replacing the method calls to instantiate the network with print statements, and then checking that the output was as expected.

This method of testing was enough to produce a parser that was able to successfully load in a compliant input file.
However the parser contained a number of bugs and could be made to crash when given an invalid input file, for instance one that contained multiply defined devices, or multiple identical inputs. 
These bugs were eventually discovered and fixed, but they could have been found earlier, if a sensible approach to testing had been followed.

\subsection{The GUI}

Automated testing was not applied to the GUI because of the relatively high complexity involved in creating automated tests for graphical components.
Instead the GUI was tested by inspection, trying every obvious combination of inputs in an attempt to produce a crash.

This technique was able to find a number of issues within the GUI. 
It was however a relatively time consuming exercise compared to testing other components.  

\section{Teamwork}
The group operated relatively well as a team, collaborating both within and outside the specified sessions.
Work was well partitioned, with each programmer being clear what their responsibilities were (see section \ref{sec:blame}).
Despite this, there was also a large degree of collaboration between developers, for instance when agreeing on a specification for the \texttt{symbol} class used for communication between the scanner and parser components. 
Additionally pair programming was employed during the implementation of some of the components, such as the internationalisation maintenance request, this was largely developed in a tandem session involving Hafiz and myself. 
 
\subsection{Source Code Control}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{bitbucket}
\caption{The BitBucket Web Interface}
\end{figure}

In order to synchronise source code between developers and track changes to the code, we decided to use git, a distributed revision control system.
 We chose to host our main git repository via bitbucket.org, this gave us access to a web interface for communicating between developers, the ability to comment on each others code when performing reviews, and pull requests: a framework for performing code reviews.

Due to time constraints the level of code review was somewhat limited, however some code review was carried out, and this did lead to bugs being identified and successfully fixed. 

Git was highly successful as a method of synchronising changes between developers. Each developer could \emph{fork}, creating a branch in the code, and make any changes they desired within this branch.
When the work carried out in this branch was completed, the changes made in the branch could be merged back into the trunk. 
Git automatically took care of merging changes made to the same file by different developers. 

\subsection{Coding Standards}

At the start of the project, a coding standard was agreed on by the team.
Most aspects of this coding standard were closely conformed to. 
The first major deviation from the coding standard was the use of PascalCase, rather than camelCase in the gui code, this was because the existing code used PascalCase. 
This is a feeble excuse for a deviation from a coding standard, as refactoring software (such as those built into a modern IDE like eclipse), allows variables to be renamed consistently across a code base. 

The other major coding standard violation was the use of spaces rather than tabs to indent code, in both the parser and the gui code. 
This violation was made partially for consistency with the legacy gui code, but also because the default settings in the Emacs text editor make it difficult to produce tab characters. 
Despite this issue, and the various health hazards posed by Emacs, some developers were still insistent on using it as a development environment.
Since the use of tabs instead of spaces was mandated by the same team-members who were unable to produce them, it was agreed on to change the standard to use spaces. 
Eclipse was used to standardise all source files on two space for indentation.

\subsection{Deadlines}
Some of the deadlines set for the project were met with a fairly narrow margin, namely the requirement to have a working simulator by the deadline for the second report.  

If areas of work had been less strictly allocated, this might have lead to a situation where programmer time was utilized more effectively, due to people working on whatever tasks needed doing most urgently at any particular moment, as opposed to only working on their specific components.  

However having less rigorously defined area of responsibility may have spread each developer too thinly, and prevented them from gaining an in depth understanding of any one component. 
This might also have lead to a situation where a particular portion of work was simply not carried out, because no one thought that it was their job.

In addition there would be the risk that developers would cluster together on one particular interesting problem, and fail to expend effort on other areas.
While pair programming was found to be an effective technique when working on bug-fixes, and on the maintenance features, it might not have been appropriate for the earlier stages of the project, when larger quantities of code needed producing.

\section{Area of Responsibility}
\label{sec:blame}
With certain deviations for minor components, Hafiz Azman was responsible for the gui component of the software, Alexander Read was in charge of the Parser component, and I produced the scanner.

When the maintenance tasks were announced, Hafiz took the localization task, since this was most related to the user interface. Alex was put in charge of implementing the new RCDEVICE device type, and I modified the code so that the circuit reset properly when the reset button was pressed. 

Various bugfixes and features, such as the localisation request and some parser bugfixes, were written by multiple developers using a pair programming techniques.  

In addition to my allocated features, I also produced the code that draws the monitor traces in multiple colours with dotted lines to mark out each simulation timestep, the code that makes the monitor display a blue line for monitor points that hadn't been recorded as the monitor had just been added and the code to add scrollbars to the monitor section of the user interface once the traces on the monitor become too large to fit on a window.   

I also modified the user interface that prompts the user to enter a location for a new monitor point, to make it a list of possible monitor points, as opposed to a text field. 

I also implemented a number of bug fixes outside the scope of the scanner, such as the traces not always being drawn in circumstances when they should, such as immediately after creation and a number of small parser bugs, such as the inability to cope with multiply defined inputs or devices.    

\section{Changes Made to the Supplied Source Code}

A number of modifications were  made to the supplied source code in order to implement the maintenance features. 
In addition to these I also recognised a bug in the monitor class that caused  data returned by a monitor to become incorrect if a monitor had been removed, but the monitors had not been reset. 

\begin{lstlisting}
void monitor::remmonitor (name dev, name outp, bool& ok)
{
  int i, j;
  bool found;
  ok = (mtab.used > 0);
  if (ok) {
    found = false;
    for (i = 0; i < mtab.used; i++){
      if((mtab.sigs[i].devid == dev) && (mtab.sigs[i].op->id == outp)) {
        found = true;
        break;
      }
    }
    ok = found;
    if (found) {
      (mtab.used)--;
      for(j = i; j < mtab.used; j++){
        mtab.sigs[j] = mtab.sigs[j + 1];
        memmove( disp[j], disp[j+1], sizeof( signaltrace ) );
      }
    }
  }
}
\end{lstlisting}

The fix for this bug was to add line 19 to the above listing. 
This moves the monitor history along with the rest of the monitor information when a monitor is removed. 

In addition to this the disp array was filled with invalid values when a monitor was created, this allowed traces to be displayed for monitors that were added after the simulation had started.
The gui was modified so that it recognises that it is looking at a monitor where part of the signal history isn't available.
The Render routine was changed so that it draws a thin blue line, instead of a signal trace, if this occurs. 

Eclipse was used to convert all of the enum member names in the legacy code to uppercase, because the previous convention of using lowercase names for enum members was frankly insane. 

\section{Conclusion \& Areas for Improvement}

Overall development of the logic simulator was a success. As the simulator was developed on time, and was fully functional. 

Collaboration between developers was assisted by source code control, this was a great success. 

Possible areas for improvement would be to agree on a more unified testing strategy, and produce a larger number of component tests for each component.
Idle team members could have been employed writing tests for other components. 
Given significantly more time, it would have been nice to look at creating automated tests for the gui.

Given a larger amount of time, it would have been interesting to experiment with different methods of allocating work to each developer, and investigating the relative methods of working on separate components, as opposed to working as a slightly knit group. 

The coding standard could have been designed with more foresight about the development environments used by each team member. 
The coding standard could also have been designed around the legacy code. 
This issue did not lead to a significant loss in productivity, and should not be considered significant. 

An ideas for a feature that we could have implemented, but didn't, is the ability to define new devices as combinations of existing devices, in order to concisely express logic circuits that contain lots of repeating elements. 
Additionally we could have produced a separate view, showing a graphical representation of the logic circuit, and displaying the current state, opposed to a graph of the state over time. 
We could also have added code to run the simulation slowly, advancing a certain number of simulation steps each second, and producing an animated trace.

\newpage
\appendix
\pagenumbering{Roman}

\section{Listings}
These listings only include code that I personally developed.

\subsection{Main Codebase}
\subsubsection{errorlog.h}
\lstinputlisting{errorlog.h}
\subsubsection{errorlog.cc}
\lstinputlisting{errorlog.cc}
\newpage


\subsubsection{gui.h}
\lstinputlisting{gui.h}
\subsubsection{gui.cc}
\lstinputlisting{gui.cc}
\newpage

\subsubsection{inputexception.h}
\lstinputlisting{inputexception.h}
\newpage

\subsubsection{inputexception.cc}
\lstinputlisting{inputexception.cc}
\newpage

\subsubsection{names.h}
\lstinputlisting{names.h}
\newpage

\subsubsection{names.cc}
\lstinputlisting{names.cc}
\newpage

\subsubsection{scanner.h}
\lstinputlisting{scanner.h}
\subsubsection{scanner.cc}
\lstinputlisting{scanner.cc}
\newpage

\subsubsection{symbol.h}
\lstinputlisting{symbol.h}
\newpage

\subsubsection{symbol.cc}
\lstinputlisting{symbol.cc}
\newpage

\subsubsection{parser.h}
This does not represent the entirety of the parser class, just the helper functions that I contributed.
\lstinputlisting{parserfragment.h}

\subsubsection{parser.cc}
\lstinputlisting{parserfragment.cc}
\newpage

\subsubsection{Makefile}
\lstinputlisting[language=make]{aMakefile}
\newpage

\subsection{Test Code}
\label{sec:testing}
\subsubsection{errorlogtest.cc}
\lstinputlisting{errorlogtest.cc}

\subsubsection{inputexceptiontest.cc}
\lstinputlisting{inputexceptiontest.cc}
\newpage

\subsubsection{namestest.cc}
\lstinputlisting{namestest.cc}
\newpage

\subsubsection{scannertest.cc}
\lstinputlisting{scannertest.cc}

\newpage
\subsection{Modified Legacy Code}

\subsubsection{devices.h}
\lstinputlisting{devices.h}

\subsubsection{devices.cc}
\lstinputlisting{devices.cc}

\subsubsection{monitor.h}
\lstinputlisting{monitor.h}

\subsubsection{monitor.cc}
\lstinputlisting{monitor.cc}

\subsubsection{network.h}
\lstinputlisting{network.h}

\subsubsection{network.cc}
\lstinputlisting{network.cc}

\newpage

\section{Attached User Guide }

\newpage

\section{Example Definition Files}

\subsection{Example 1}
\begin{framed}
\texttt{
\\
SWITCH d\_switch \{\\
\indnt initial = 0;\\
\}\\
\\
SWITCH s\_switch\{\\
\indnt initial = 0;\\
\} \\
\\
RCDEVICE rc\_dev \{\\
\indnt timeconstant = 5;\\
\} \\
\\
CLOCK a\_clock \{\\
\indnt period = 5;\\
\} \\
\\
DTYPE a\_flip\_flop \{\\
\indnt DATA = d\_switch;\\
\indnt CLK = a\_clock;\\
\indnt SET = s\_switch;\\
\indnt CLEAR = rc\_dev;\\
\} \\
\\
MONITOR display \{\\
\indnt I = a\_clock;\\
\indnt I = a\_flip\_flop.Q;\\
\}\\
}

\end{framed}

\begin{figure}[H]
\centering
\begin{circuitikz} \draw
(0.5,1.5) to[cspst] (2,1.5)
(0.5,3) to[cspst] (2,3)
;

\draw( 0, 2.5 ) rectangle( 2, 3.5 );
\draw( 0, -1.0 ) rectangle( 2, -2.0 );


\draw( 0, 1.0 ) rectangle( 2, 2.0 );
\draw node[align=center] at (1,0.0){Clock\\Period=5};
\draw( 0, -0.5 ) rectangle( 2, 0.5 );

\draw node[align=center] at (1,-1.5){RC Device\\Constant=5};

\draw( 3, 0 ) node[cross=5pt, red, thick]{};

\draw( 4, -0.5 ) rectangle +( 2.0, 2.5 );  
\draw (4, -0.2) -- ( 4.4, 0.0) -- ( 4.0, 0.2);
\draw (4.2, 1.5 )node[align=center]{D};
\draw (5, 1.8 )node[align=center]{Set};
\draw (5, -0.3 )node[align=center]{Reset};
\draw (5.8, 1.5 )node[align=center]{Q};
\draw (5.8, 0.0 )node[align=center]{$\bar{\textrm{Q}}$};

\draw (2, 0.0) -- ( 4.0, 0.0);
\draw (2, 1.5) -- ( 4.0, 1.5);

\draw (2, -1.5) -| ( 5.0, -0.5);
\draw (2, 3) -| ( 5.0, 2);

\draw (6, 0.0) -- ( 7.0, 0.0);
\draw (6, 1.5) -- ( 7.0, 1.5);

\draw( 6.5, 1.5 ) node[cross=5pt, red, thick]{};

\draw( 2, -2.5 ) node[cross=5pt, red, thick, align=left](key){};
\draw (3.6, -2.53 )node[align=center]{Monitoring Point};

\end{circuitikz} 
\caption{$1^{st}$ Example Logic Circuit}
\label{fig:example1}
\end{figure}

\newpage

\subsection{Example 2}

\begin{framed}
\texttt{\\
CLOCK C1 \{\\
\indnt period = 5;\\
\} \\
CLOCK C2\{\\
\indnt period = 9;\\
\} \\
SWITCH S\{\\
\indnt initial = 1;\\
\}\\
AND G1\{\\
\indnt I1 = S;\\
\indnt I2 = C1;\\
\}\\
NOR G2\{\\
\indnt I1 = G1;\\
\indnt I2 = S;\\
\indnt I3 = C2;\\
\}\\
XOR G3\{\\
\indnt I1 = C1;\\
\indnt I2 = G5;\\
\}\\
OR G4\{\\
\indnt I1 = C1;\\
\indnt I2 = G1;\\
\indnt I3 = G3;\\
\} \\
NAND G5\{\\
\indnt I1 = G4;\\
\indnt I2 = G3;\\
\}\\
MONITOR monitor\{\\
\indnt I1 = C1;\\
\indnt I2 = C2;\\
\indnt I3 = G4;\\
\} \\
}
\end{framed}

\begin{figure}[H]
\centering
\begin{circuitikz}\draw
(0.5,0.0) to[ospst] (2,0.0)

(4,1) node[and port] (myand) {}
(6,-1) node[nor port] (mynor) {}
(8,-1) node[xor port] (myxor) {}
(10,0) node[or port] (myor) {}
(12,-1.0) node[nand port] (mynand) {}


(2,2.0) -| (myand.in 1)
(2,0.0) -| (myand.in 2)


(myand.out) -| (mynor.in 1)
(2, 0 ) -| ( 4, -1 ) (4, -1) -| (5.08, -1 )
(2, -2) -| (mynor.in 2)


(2,2.0) -| (myxor.in 1)

(2,2.0) -| (myor.in 1)
(myand.out) -| (8, 0) (8, 0) -| (9.08, 0)
(myxor.out) -| (myor.in 2)

(myor.out) -| (mynand.in 1)

(mynor.out) |- ( 8, -2) 
( 8, -2) -| (mynand.in 2)


(mynand.out) |- ( 8, -2.5) 
( 8, -2.5) -| (myxor.in 2)
;


\draw( 0, -1.5 ) rectangle( 2, -2.5 );
\draw node[align=center] at (1,-2.0){Clock\\Period=9};

\draw( 0, 1.5 ) rectangle( 2, 2.5 );
\draw node[align=center] at (1,2.0){Clock\\Period=5};

\draw( 0, -0.5 ) rectangle( 2, 0.5 );


\draw( 3.3, -2 ) node[cross=5pt, red, thick, align=left](clock1){};
\draw( 2.3,  2 ) node[cross=5pt, red, thick, align=left](clock2){};

\draw( 10.2, 0 ) node[cross=5pt, red, thick, align=left](output){};

\draw( 4, -3.2 ) node[cross=5pt, red, thick, align=left](key){};
\draw (5.6, -3.23 )node[align=center]{Monitoring Point};

\end{circuitikz} 
\caption{$2^{nd}$ Example Logic Circuit}
\label{fig:example2}
\end{figure}

\newpage
\section{File Format}
The final EBNF specification is as follows;

\begin{framed}
\texttt{
\\
NETWORK = \{ device \}\\\\
device = type\_name device\_name "\{" \{ parameter \} "\}"\\\\
type\_name = "CLOCK" | "RCDEVICE" | "SWITCH" | "AND" | "NAND" | "OR" | "NOR" | "DTYPE" | "XOR" | "MONITOR"\\\\
parameter = ( input | property | timeconstant ) ";" \\\\
input = input\_name "=" output\_id \\\\
property = property\_name "=" value \\\\
device\_name = letter \{ letter | digit | "\_" \}\\\\
input\_name = ( "I" number ) | "DATA" | 'CLK" | "SET" | "CLEAR" | * \\\\
output\_id = device\_name [ "." output\_name ] \\\\
output\_name = Q | QBAR \\\\
property\_name = "initial" | "period"\\\\
value = digit \{ digit \}\\\\
letter = "A" | "B" | "C" | "D" | "E" | "F" | "G"
       | "H" | "I" | "J" | "K" | "L" | "M" | "N"
       | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
       | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
       | "c" | "d" | "e" | "f" | "g" | "h" | "i"
       | "j" | "k" | "l" | "m" | "n" | "o" | "p"
       | "q" | "r" | "s" | "t" | "u" | "v" | "w"
       | "x" | "y" | "z" \\\\
digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" \\
}
\end{framed}

In addition to this c and c++ style comments are permitted within the file.
C style comments are intended to mean a comment that begins with the two characters \texttt{/*}, and ends when the characters  \texttt{*/} are found. 
Recursive c style comments are not supported, the comment will end at the first occurrence of \texttt{*/}.
C++ style comments are intended to be interpreted as a comment that starts with the characters \texttt{//} and continues until the next line.

Comments are implemented in the scanner, at the \texttt{getNextChar} level. All file access is done via this function.
As a result of this, it is perfectly legal to have a comment within a name or keyword, splitting this token into two parts. 

\newpage

\section{Files in the Team Directory}

\begin{table}[H]
\centering
\begin{tabular}{ r l }
\toprule
Filename & Purpose \\
\midrule
devices.cc & modified devices class definition\\
 devices.h& modified devices class declaration\\
 errorlog.cc& definition of the error log singleton\\
 errorlog.h& declaration of the error log singleton\\
errorlogtest.cc& test of the error log singleton\\
 gtest-1.7.0& test framework\\
 gui.cc& definition of the graphical user interface\\
 gui.h& declaration of the graphical user interface\\
 guitest.cc& legacy UI test code\\
 guitest.h& legacy UI test code\\
inputexception.cc&definition of the exception class\\
 inputexception.h& declaration of the exception class\\
 inputexceptiontest.cc& tests of the exception class\\
languages.cc& definition of classes used in localisation\\
 languages.h& declaration of classes used in localisation\\
 lexical\_analysis\_test& the scanner test suite \\
 logsim& the compiled application\\
logsim.cc& entry point definition\\
 logsim.h& entry point declaration\\
 Makefile& make script to build the application\\
 monitor.cc& modified monitor class definition\\
 monitor.h& modified monitor class declaration\\
names.cc& definition of the name classes\\
 names.h& declaration of the name classes\\
 namestest.cc& test definitions for the name classes \\
 network.cc& modified network class definition\\
 network.h& modified network class declaration\\
parser.cc& the parser class definition\\
 parser.h& the parser class declaration\\
 scanner.cc& the scanner class definition\\
 scanner.h& the scanner class declaration\\
 scannertest.cc& test definitions for the scanner\\
symbol.cc& class used to communicate between the scanner and parser\\
 symbol.h& class used to communicate between the scanner and parser\\
 tf2& sample definition file\\
 tf3& sample definition file\\
 tf4& sample definition file\\
 tf5& sample definition file, causes the network to oscillate \\
 tf6& sample definition file\\
 tf7& sample definition file\\
userint.cc& textual UI for the program\\
userint.h& header file for the textual UI\\
wx\_icon.xpm& icon file used by the application\\
\bottomrule
\end{tabular} 
\caption{Files in the Team Directory} 
\label{tab:Directoryfiles}
\end{table}

\end{document}
