#include <sstream>

#include "inputexception.h"

using namespace std;

const int inputException::REASON_COUNT = 18;
const char * inputException::REASONS[] = {
	"This reason has been entered in error",
	"A semicolon was expected, and wasn't found",
	"An invalid type name was found",
	"An invalid device name was found",
	"An invalid property name was found",	
	"An invalid input name was found",
	"A keyword was used as a device name",
	"A open brace was expected, and was not found",
	"A close brace was expected, and was not found",
	"An equals sign was expected, and was not found",
	"An input was used which is not appropriate for this device",
	"The same device name was used in multiple places",
 	"An output was refered to which is non existent",
	"A device was reffered to which is non existent",
	"A property that was expected is missing",
	"An invalid property was found",
	"Too many monitering points - the maximum is 10",
	"A device name was expected, but was not found"
} ; 
 
inputException::inputException( inputException::inputErrorType aType, 
	pair<int, int> position, string aLine ){
	theLine = aLine;
	theLineNumber = position.first;
	theCharNumber = position.second;
	if( aType < 0 || aType >= REASON_COUNT ){
		theType = INVALID;
	} else {
		theType = aType;
	}
}

/* Not Used */
inputException::inputException(){};

string inputException::getReasonString(){
	return string( REASONS[ theType ] );
}

string inputException::getLineString(){
	stringstream s;
	s << "An error occured on line " << theLineNumber << ":" <<endl;
	s << theLine << endl;
	for( int i = 0; i< theCharNumber; i++ ){
		s << " ";
	}
	s << "^" << endl;
	return s.str();
}
