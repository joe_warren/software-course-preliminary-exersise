#include <sstream>

#include "inputexception.h"
#include "languages.h"

using namespace std;

const int inputException::REASON_COUNT = 19;

inputException::inputException( inputException::inputErrorType aType, 
    pair<int, int> position, string aLine ){
  theLine = aLine;
  theLineNumber = position.first;
  theCharNumber = position.second;
  if( aType < 0 || aType >= REASON_COUNT ){
    theType = INVALID;
  } else {
    theType = aType;
  }
}

/* Not Used */
inputException::inputException(){};

string inputException::getReasonString(int languageChoice){
  return string( languages::langs[languageChoice]
                                  [ theType + languages::INPUT_EXCEPTION_OFFSET ] );
}

string inputException::getLineString(int languageChoice){
  stringstream s;
  s << languages::langs[languageChoice][languages::MSG_ERROR_ON_LINE] << theLineNumber << ":" <<endl;
  s << theLine << endl;
  for( int i = 0; i< theCharNumber; i++ ){
    s << " ";
  }
  s << "^" << endl;
  return s.str();
}
