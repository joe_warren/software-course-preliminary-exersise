#ifndef errorlog_h
#define errorlog_h

#include <vector>
#include "inputexception.h"
namespace std{

class errorLog: public vector<inputException> {
	public:
		static errorLog& getInstance();
	private:
		errorLog();
		errorLog(errorLog const&);			// Non Implemented
		void operator=(errorLog const&);	// Non Implemented
};

}

#endif /* errorlog_h */
